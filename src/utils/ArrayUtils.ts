export const first: <T>(arg: T[]) => T = <T>(arg: T[]): T => arg[0];
export const last: <T>(arg: T[]) => T = <T>(arg: T[]): T => arg[arg.length - 1];
