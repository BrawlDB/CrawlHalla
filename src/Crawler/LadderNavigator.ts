import { BrawlhallaApi } from 'brawlhalla-api-ts';
import { validDivision, validTier } from 'brawlhalla-api-ts/models/rankingOptions';
import { PlayerRanking } from 'brawlhalla-api-ts/models/search/PlayerRanking';
import { first, last } from '../utils/ArrayUtils';
import { ONE_SECOND, sleep } from '../utils/TimeUtils';
import * as fs from 'fs';
import { eloToTier } from '../utils/RankedUtils';

export class LadderNavigator {
  public static readonly CRAWLED_FILE_NAME: string = 'ladder/crawled.json';
  private static readonly playersPerPage: number = 50;
  private static tiers: validTier[] = ['Tin', 'Bronze', 'Silver', 'Gold', 'Platinum', 'Diamond'];
  // tslint:disable-next-line:no-magic-numbers
  private static divisions: validDivision[] = [0, 1, 2, 3, 4, 5];

  /**
   * Number of MS between each requests
   */
  public crawlDelay: number;

  private currentPage: number;
  private minPage: number;
  private maxPage: number;
  private crawlStep: number;
  private crawledPages: Map<number, PlayerRanking[]>;

  private lastCrawlTime: number;

  private bhApi: BrawlhallaApi;

  public constructor(config: { apiKey: string; crawlDelay?: number }) {
    this.bhApi = new BrawlhallaApi(config.apiKey);
    // tslint:disable-next-line:no-magic-numbers
    this.crawlDelay = config.crawlDelay !== undefined ? config.crawlDelay : 400;

    this.lastCrawlTime = 0;
    this.currentPage = 1;
    this.minPage = 1;
    this.maxPage = 0;
    // tslint:disable-next-line:no-magic-numbers
    this.crawlStep = 100;
    this.crawledPages = new Map();
    this.loadCrawledPages();
  }

  public async getLadder(): Promise<void> {
    return this.getAllPages();
  }

  private async getAllPages(): Promise<void> {
    const oldCrawlStep: number = this.crawlStep;

    this.currentPage = 1;
    let currentPageResult: PlayerRanking[];
    do {
      console.log(`Time: ${Date.now()}, Page: ${this.currentPage}`);
      currentPageResult = await this.getPage(this.currentPage);
      this.currentPage += 1;
    } while (currentPageResult.length > 0);
    this.crawlStep = oldCrawlStep;
  }

  private async getTierInformation(tierName: validTier): Promise<ITierResult> {
    const currentPageResult: PlayerRanking[] = await this.getPage(this.currentPage);
    throw new Error('Unimplemented');
  }

  private isCorrectTier(tierName: validTier, pageResult: PlayerRanking[]): CrawlResponseStatus {
    if (pageResult.length === 0) {
      return CrawlResponseStatus.TooHigh;
    }
    const firstPlayer: PlayerRanking = first<PlayerRanking>(pageResult);

    if (firstPlayer.tier === tierName) {
      return CrawlResponseStatus.Correct;
    }

    return CrawlResponseStatus.TooLow;
  }

  private async getPage(pageNumber: number): Promise<PlayerRanking[]> {
    if (!this.crawledPages.has(pageNumber)) {
      let pageResult: PlayerRanking[] | undefined;
      do {
        await this.waitCrawlDelay();
        try {
          pageResult = await this.bhApi.getRankings({ page: pageNumber });
        } catch (exception) {
          console.warn(exception);
        }
      } while (pageResult === undefined);
      this.crawledPages.set(pageNumber, pageResult);

      // To save API calls during development
      // TODO: Make a settings to use the cache
      this.saveCrawledPages();
    }

    // tslint:disable-next-line:no-non-null-assertion
    return Promise.resolve(this.crawledPages.get(pageNumber)!);
  }

  private saveCrawledPages(): void {
    const crawledPageObject: { [key: number]: PlayerRanking[] } = {};
    this.crawledPages.forEach((value: PlayerRanking[], key: number) => {
      crawledPageObject[key] = value;
    });
    fs.writeFileSync(LadderNavigator.CRAWLED_FILE_NAME, JSON.stringify(crawledPageObject));
  }

  private loadCrawledPages(): void {
    let crawledPageObject: { [key: number]: PlayerRanking[] };
    if (fs.existsSync(LadderNavigator.CRAWLED_FILE_NAME)) {
      const crawledJson: string = fs.readFileSync(LadderNavigator.CRAWLED_FILE_NAME, 'utf8');
      crawledPageObject = JSON.parse(crawledJson);
    } else {
      console.log('No cache file found!');
      crawledPageObject = {};
    }

    this.crawledPages = new Map();
    Object.keys(crawledPageObject)
      .map((key: string) => +key)
      .forEach((key: number) => {
        this.crawledPages.set(key, crawledPageObject[key]);
      });
  }

  /**
   * Waits until the crawl delay is over before resolving
   */
  private async waitCrawlDelay(): Promise<void> {
    const sleepDelay: number = this.lastCrawlTime + this.crawlDelay - Date.now();
    await sleep(sleepDelay);
    this.lastCrawlTime = Date.now();
  }
}

export interface ICrawlResult {
  tiers: ITierResult[];
}

export interface ITierResult {
  name: validTier;
  firstPosition: number;
  lastPosition: number;
  divisions: IDivisionResult[];
}

export interface IDivisionResult {
  number: validDivision;
  firstPosition: number;
  lastPosition: number;
}

export interface ICrawlOptions<T> {
  currentPage: number;
  minPage?: number;
  maxPage?: number;
  step?: number;

  /**
   * Finds out if the given page is too high, low or is the correct page
   * @param pageResult The crawled page result
   * @return CrawlResponseStatus The status of the current crawled page
   */
  condition(pageResult: PlayerRanking): CrawlResponseStatus;

  /**
   * Extracts the required result from the current page
   * @param {PlayerRanking} pageResult The current page result
   * @return {T} The extracted result
   */
  resultExtractor(pageResult: PlayerRanking): T;
}

export enum CrawlResponseStatus {
  TooHigh,
  TooLow,
  Correct
}
