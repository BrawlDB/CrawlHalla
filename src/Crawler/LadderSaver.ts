import { connect, Document, model, Model, mongo, Mongoose, Schema } from 'mongoose';
import { PlayerRanking } from 'brawlhalla-api-ts/models/search/PlayerRanking';
import { LadderNavigator } from './LadderNavigator';
import * as fs from 'fs';
import { validDivision, validTier } from 'brawlhalla-api-ts/models/rankingOptions';
import { ONE_SECOND, sleep } from '../utils/TimeUtils';

const playerSchema: Schema = new Schema({
  bhid: {
    type: Number,
    unique: true
  },
  name: String,
  legend: {
    id: Number,
    name: String,
    games: Number,
    wins: Number
  },
  elo: Number,
  peakElo: Number,
  tier: String,
  division: Number,
  games: Number,
  wins: Number,
  region: String,
  twitch: String
});
// tslint:disable-next-line:variable-name
const PlayerModel: Model<IPlayer> = model<IPlayer>('Player', playerSchema);

export class LadderSaver {
  private static readonly MONGO_CONNECTION_STRING: string = 'mongodb://127.0.0.1/crawlhalla';

  private mongoConnection: Promise<Mongoose>;
  private playerModel?: Model<IPlayer>;
  private crawledPlayers: Map<number, PlayerRanking>;

  private savePromiseChain: Promise<void>;

  public constructor() {
    this.mongoConnection = connect(LadderSaver.MONGO_CONNECTION_STRING);
    this.mongoConnection.then((m: Mongoose) => {
      this.playerModel = m.model<IPlayer>('Player', playerSchema);
    });
    this.crawledPlayers = this.loadCrawledPlayers();
    this.savePromiseChain = Promise.resolve();
  }

  public async saveToMongo(): Promise<void> {
    console.log('Waiting connection');
    await this.mongoConnection;

    console.log('Start saving players');
    // tslint:disable-next-line
    // this.convertAndSavePlayer(this.crawledPlayers.get(931071)!);
    const playerIterator: IterableIterator<PlayerRanking> = this.crawledPlayers.values();
    let player: IteratorResult<PlayerRanking> = playerIterator.next();
    let savedPlayerCount: number = 0;
    while (!player.done) {
      await this.convertAndSavePlayer(player.value);
      player = playerIterator.next();
      savedPlayerCount++;
      // tslint:disable-next-line:no-magic-numbers
      if (savedPlayerCount % 100 === 0) {
        console.log(`Saved ${savedPlayerCount} / ${this.crawledPlayers.size}`);
      }
    }
    // this.crawledPlayers.forEach((player: PlayerRanking) => {
    //   console.log(`attempt player ${player.name}`);
    //   this.convertAndSavePlayer(player);
    // });
    console.log('Done saving players');
  }

  private async convertAndSavePlayer(player: PlayerRanking): Promise<void> {
    const mongoPlayer: IPlayer = this.convertPlayerRankingToMongo(player);
    await this.savePlayer(mongoPlayer);
  }

  private async savePlayer(mongoPlayer: IPlayer): Promise<void> {
    const savedPlayer: IPlayer = await mongoPlayer.save();
  }

  private convertPlayerRankingToMongo(player: PlayerRanking): IPlayer {
    // tslint:disable-next-line:no-non-null-assertion
    return new this.playerModel!({
      bhid: player.brawlhallaID,
      name: player.name,
      legend: {
        id: player.bestLegend.id,
        name: player.bestLegend.name,
        games: player.bestLegendGames,
        wins: player.bestLegendWins
      },
      elo: player.rating,
      peakElo: player.peakRating,
      tier: player.tier.split(' ')[0],
      division: player.tier.split(' ')[1] || 0,
      games: player.games,
      wins: player.wins,
      region: player.region
    });
  }

  private loadCrawledPlayers(): Map<number, PlayerRanking> {
    console.log('Loading players!');
    let crawledPageObject: { [key: number]: PlayerRanking[] };
    if (fs.existsSync(LadderNavigator.CRAWLED_FILE_NAME)) {
      const crawledJson: string = fs.readFileSync(LadderNavigator.CRAWLED_FILE_NAME, 'utf8');
      crawledPageObject = JSON.parse(crawledJson);
    } else {
      console.log('No cache file found!');
      crawledPageObject = {};
    }

    const crawledPlayers: Map<number, PlayerRanking> = new Map();
    Object.keys(crawledPageObject)
      .map((key: string) => crawledPageObject[+key])
      .reduce((acc: PlayerRanking[], page: PlayerRanking[]) => acc.concat(page), [])
      .forEach((player: PlayerRanking) => {
        crawledPlayers.set(player.brawlhallaID, player);
      });
    console.log('Done loading players!');

    return crawledPlayers;
  }
}

export interface IPlayer extends Document {
  bhid: number;
  name: string;
  legend: {
    id: number;
    name: string;
    games: number;
    wins: number;
  };
  elo: number;
  peakElo: number;
  tier: validTier;
  division: validDivision;
  games: number;
  wins: number;
  region: string;
  twitch?: string;
}

new LadderSaver().saveToMongo();
