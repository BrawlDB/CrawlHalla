import { validDivision, validTier } from 'brawlhalla-api-ts/models/rankingOptions';

export const DIAMOND_MINIMUM_ELO: number = 2000;
export const PLATINUM_MINIMUM_ELO: number = 1680;
export const GOLD_MINIMUM_ELO: number = 1390;
export const SILVER_MINIMUM_ELO: number = 1130;
export const BRONZE_MINIMUM_ELO: number = 910;
export const TIN_MINIMUM_ELO: number = 720;

export const DIAMOND_TIER_STEP: number = 5000;
export const PLATINUM_TIER_STEP: number = 63;
export const GOLD_TIER_STEP: number = 57;
export const SILVER_TIER_STEP: number = 51;
export const BRONZE_TIER_STEP: number = 42;
export const TIN_TIER_STEP: number = 37;

// TODO: Convert this into a real class
type TIER_TYPE = [validTier, number, number];
const TIER_NAME_INDEX: 0 = 0;
const TIER_MINIMUM_ELO_INDEX: 1 = 1;
// WTF TSLint, defining the constant here
// tslint:disable-next-line:no-magic-numbers
const TIER_STEP_INDEX: 2 = 2;

const PLATINUM_TIER: TIER_TYPE = ['Platinum', PLATINUM_MINIMUM_ELO, PLATINUM_TIER_STEP];
const GOLD_TIER: TIER_TYPE = ['Gold', GOLD_MINIMUM_ELO, GOLD_TIER_STEP];
const SILVER_TIER: TIER_TYPE = ['Silver', SILVER_MINIMUM_ELO, SILVER_TIER_STEP];
const BRONZE_TIER: TIER_TYPE = ['Bronze', BRONZE_MINIMUM_ELO, BRONZE_TIER_STEP];
const TIN_TIER: TIER_TYPE = ['Tin', TIN_MINIMUM_ELO, TIN_TIER_STEP];
const TIERS_ELO_AND_STEP: TIER_TYPE[] = [
  PLATINUM_TIER,
  GOLD_TIER,
  SILVER_TIER,
  BRONZE_TIER,
  TIN_TIER
];

/**
 * Converts an elo (numeric) to its pure tier and division
 * @param elo The elo to convert
 * @return The matching tier and division
 */
export const eloToTier: (elo: number) => [validTier, validDivision] = (
  elo: number
): [validTier, validDivision] => {
  if (elo >= DIAMOND_MINIMUM_ELO) {
    return ['Diamond', 1];
  } else if (elo < TIN_MINIMUM_ELO) {
    return ['Tin', 0];
  }

  // Find the current tier, or fallback to Tin if elo is too low
  const currentTier: TIER_TYPE =
    TIERS_ELO_AND_STEP.find((tier: TIER_TYPE): boolean => elo >= tier[TIER_MINIMUM_ELO_INDEX]) ||
    TIN_TIER;

  // (elo - min elo for tier) / tier steps
  const currentDivision: validDivision = ((elo - currentTier[TIER_MINIMUM_ELO_INDEX]) /
    currentTier[TIER_STEP_INDEX]) as validDivision;

  return [currentTier[TIER_NAME_INDEX], currentDivision];
};
