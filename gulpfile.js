var gulp = require('gulp');
var ts = require('gulp-typescript');
var sourcemaps = require('gulp-sourcemaps');
var merge = require('merge-stream');

var tsProject = ts.createProject('./tsconfig.json');

gulp.task('default', function () {
  var tsResult = tsProject.src()
    .pipe(sourcemaps.init())
    .pipe(tsProject());
  return merge(tsResult, tsResult.js)
    .pipe(sourcemaps.write('.', { sourceRoot: './src' }))
    .pipe(gulp.dest('./dist'));
});
