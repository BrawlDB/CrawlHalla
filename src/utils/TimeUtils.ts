export const sleep: (delay: number) => Promise<void> = async (delay: number): Promise<void> => {
  await new Promise((resolve: () => void): void => {
    setTimeout(resolve, delay);
  });
};

export const ONE_SECOND: number = 1000;
