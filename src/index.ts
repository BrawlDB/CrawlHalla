import * as fs from 'fs';
import { ICrawlResult, LadderNavigator } from './Crawler';

const configJson: string = fs.readFileSync('config.json', 'utf8');
const configObject: { BrawlhallaApiKey: string } = JSON.parse(configJson);

if (configObject.BrawlhallaApiKey === undefined) {
  throw new Error('Invalid API key given!');
}

const crawler: LadderNavigator = new LadderNavigator({
  apiKey: configObject.BrawlhallaApiKey,
  crawlDelay: 1000
});

const resolver: () => Promise<void> = async (): Promise<void> => {
  await crawler.getLadder();
};

resolver();
